import sys
import numpy as np
import matplotlib.pyplot as plt


class Kplot:

    # *******************
    # class constructor set path attribute
    def __init__(self, path="."):
        self.path = path

    # ***********************
    def plot1(self, species=None):
        if species is None:
            species = ["E", "H2", "H", "H2O", "OH", "O", "O+", "OH+", "H2O+",
                       "H3O+"]
        labs = ["time/yr"] + species
        self.data = self.load(self.path + "/evolution.dat", labs)
        self.plot("time/yr", ["H2O", "H3O+", "E"])

    # ***********************
    def plot2(self, species=None):
        if species is None:
            species = ["E", "H-", "H2", "H", "H2O", "OH", "O", "O+", "OH+", "H2O+",
                       "H3O+", "H2+", "H+", "H3+"]
        labs = ["time/yr"] + species
        self.data = self.load(self.path + "/evolution.dat", labs)
        self.plot("time/yr", ["H2O", "H3O+", "E", "H3+"])

    # ***********************
    def plot3(self):
        labs = ["xvar", "radius", "density", "Tgas"]
        self.data = self.load(self.path + "/density.out", labs)
        self.plot("radius", "density")

    # ***********************
    def plot4(self):
        labs = ["time/s", "radius/cm", "energy/eV", "Jflux/(eV/cm2)", "opacity"]
        self.data = self.load(self.path + "/radiation.out", labs)
        xmin = min(self.data["time/s"])
        idxs = np.where(self.data["time/s"] == xmin)
        self.data = {k: v[idxs] for k, v in self.data.items()}
        self.splot("energy/eV", "radius/cm", "Jflux/(eV/cm2)", time=False)

    # ***********************
    def plot5(self, species=None):
        if species is None:
            species = ["E", "H-", "H2", "H", "H2O", "OH", "O", "O+", "OH+", "H2O+",
                       "H3O+", "H2+", "H+", "H3+"]
        labs = ["time/s", "radius/cm", "density/(g/cm3)", "Tgas/K"] + species
        self.data = self.load(self.path + "/density.out", labs)
        self.splot("time/s", "radius/cm", "H2O")

    # ***********************
    def plot6(self, species=None):
        if species is None:
            species = ["E", "H-", "H2", "H", "H2O", "OH", "O", "O+", "OH+", "H2O+",
                       "H3O+", "H2+", "H+", "H3+"]
        labs = ["time/s", "radius/cm", "density/(g/cm3)", "Tgas/K"] + species
        self.data = self.load(self.path + "/density.out", labs)
        self.splot("time/s", "radius/cm", "Tgas/K")

    # ***********************
    def plot7(self, species=None):
        self.plot5(species)

    # ***********************
    def plot8(self, species=None):
        if species is None:
            species = ["E", "H-", "H2", "H", "H2O", "OH", "O", "H2O_dust", "O+", "OH+",
                       "H2O+", "H3O+", "H2+", "H+", "H3+"]
        labs = ["time/yr"] + species
        self.data = self.load(self.path + "/evolution.dat", labs)
        self.plot("time/yr", ["H2O", "H2O_dust"])


    # ***********************
    @staticmethod
    def load(fname, labs):
        fdata = {x: [] for x in labs}
        for row in open(fname):
            srow = row.strip()
            # skip comments and blanks
            if srow == "":
                continue
            if srow.startswith("#"):
                continue
            # convert to floating
            arow = [float(x) for x in srow.split(" ") if x != ""]
            # check labels length wrt number of file columns
            if len(arow) != len(labs):
                sys.exit("ERROR: number of labels not matching the number of columns in the file!")
            # append data to dictionary lists
            for ii, lab in enumerate(labs):
                fdata[lab].append(arow[ii])

        # return dictionary of numpy arrays
        return {k: np.array(v) for k, v in fdata.items() if v != []}

    # ***********************
    # returns the file as a dictionary like dict["species"] = [np.array]
    @staticmethod
    def load_dump(fname, species, force):

        labs = ["xvar", "radius", "density", "Tgas"] + species
        fdata = {x: [] for x in labs}
        for row in open(fname):
            srow = row.strip()
            # skip comments and blanks
            if srow == "":
                continue
            if srow.startswith("#"):
                continue
            # convert to floating
            arow = [float(x) for x in srow.split(" ") if x != ""]
            # check labels length wrt number of file columns
            if len(arow) != len(labs) and not force:
                sys.exit("ERROR: number of labels not matching the number of columns in the file!")
            # append data to dictionary lists
            for ii, lab in enumerate(labs):
                fdata[lab].append(arow[ii])

        # return dictionary of numpy arrays
        return {k: np.array(v) for k, v in fdata.items() if v != []}

    # *******************
    def plot(self, xlab, ylabs):

        if type(ylabs) is not list:
            ylabs = [ylabs]

        for lab in ylabs:
            plt.loglog(self.data[xlab], self.data[lab], label=lab)

        plt.xlabel("log(" + xlab + ")")
        plt.ylabel("log(" + ", ".join(ylabs) + ")")

        plt.legend(loc="best")
        plt.savefig("plot.pdf")
        plt.show()

    # *******************
    def splot(self, xlab, ylab, zlab, time=True):

        if time:
            xlen = len(np.unique(self.data[xlab]))
            ylen = len(np.unique(self.data[ylab]))
        else:
            ylen = len(np.unique(self.data[xlab]))
            xlen = len(np.unique(self.data[ylab]))

        xdata = self.data[xlab].reshape((xlen, ylen))
        ydata = self.data[ylab].reshape((xlen, ylen))
        zdata = self.data[zlab].reshape((xlen, ylen))

        pcol = plt.pcolor(np.log10(xdata), np.log10(ydata), np.log10(zdata))
        pcol.set_edgecolor('face')
        cs = plt.contour(np.log10(xdata), np.log10(ydata), np.log10(zdata), colors="k")
        plt.clabel(cs, inline=1, fontsize=10)
        plt.xlabel("log(" + xlab + ")")
        plt.ylabel("log(" + ylab + ")")
        plt.title("log(" + zlab + ")")
        plt.savefig("plot.pdf")
        plt.show()



