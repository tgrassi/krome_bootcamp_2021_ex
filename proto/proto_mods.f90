
!****************************
subroutine dump_density_profile(this,fileNumber,xvariable)
  use proto_commons
  implicit none
  class(proto),intent(in)::this
  real*8,intent(in)::xvariable
  integer,intent(in)::fileNumber
  integer::icell
  real*8::Tgas

  !loop on cells
  do icell=1,ncells
     Tgas = this%energy(icell) * (this%adiabaticIndex(icell) - 1d0) &
          * pmass * this%mu(icell) / kboltzmann
     write(fileNumber,'(99E17.8e3)') xvariable, this%radius(icell), &
          this%density(icell), Tgas, this%x(icell, :)
  end do
  write(fileNumber,*)

end subroutine dump_density_profile

!****************************
subroutine dump_radiation_flux(this,fileNumber,xvariable)
  implicit none
  class(proto),intent(in)::this
  real*8,intent(in)::xvariable
  integer,intent(in)::fileNumber
  integer::icell,ienergy

  !loop on cells
  do icell=1,ncells
     !loop on energy bins
     do ienergy=1,nenergy
        write(fileNumber,'(99E17.8e3)') xvariable, this%radius(icell), &
             this%energyMid(ienergy), this%Jflux(icell,ienergy), &
             this%opacity(icell,ienergy)
     end do
     write(fileNumber,*)
  end do
  write(fileNumber,*)

end subroutine dump_radiation_flux

!****************************
!initialize chemistry
subroutine init_chemistry(this)
  ! HINT: use statements goes here
  implicit none
  class(proto),intent(inout)::this

  ! HINT: here is where you have to put the initialization
  ! of KROME and set species abundances.
  ! This is the equivalent of test.f90 from the very first
  ! exercise where chemical abundances are initialized

  ! HINT: access species abundances as for example
  ! this%x(:, krome_idx_H) = 0.63
  ! In Fortran : means loop on all the elements of an array

  ! HINT: initialize photochemistry bins here

  ! HINT: load opacity tables here

end subroutine init_chemistry

!****************************
!solve chemistry, dt is the time-step in seconds
subroutine solve_chemistry(this, dt)
  use proto_commons
  ! HINT: do not forget to include KROME modules here (cfr. test.f90)
  implicit none
  class(proto),intent(inout)::this
  real*8,intent(in)::dt
  ! HINT: use the variables defined below
  real*8::Tgas, n(nchemistry), rho
  integer::i

  ! HINT: here is where you have to call KROME to solve
  ! chemistry and updates chemistry-dependent
  ! quantities as e.g. opacity

  ! HINT: this is the equivalent of the test.f90 in the very
  ! first exercise, where KROME is called

  ! HINT: loop to call KROME for each cell as
  ! do i=1,ncells
  !  Tgas = constant * this%energy(i) !to convert energy->Tgas (see eq.3)
  !  rho = this%density(i) !to get mass density for krome_x2n (see next line)
  !  n(:) = krome_x2n(...) !to convert from mass fraction to number density
  !  call KROME here to evolve chemistry <<<<<
  !  this%x(i, :) = krome_n2x(...) !to convert back to mass fraction
  !  this%energy(i) = Tgas / constant !to convert back Tgas->energy (eq.3)
  ! end do

end subroutine solve_chemistry
