module proto_class
  use proto_commons
  implicit none
  private
  type,public::proto
     !>attributes
     real*8::density(ncells),energy(ncells),radius(ncells)
     real*8::x(ncells,nchemistry),dr(ncells)
     real*8::accretionRate,starMass,radiusMin,radiusMax, &
          starRadius,energyMin,energyMax,dust2gas
     real*8::adiabaticIndex(ncells),mu(ncells)
     !radiation
     real*8::Jflux(ncells,nenergy),TeffStar
     real*8::energyLeft(nenergy),energyRight(nenergy)
     real*8::energyDelta(nenergy),energyMid(nenergy)
     real*8::opacity(ncells,nenergy)
     !dust
     real*8::xdust(ncells,ndust)
     real*8::Tdust(ncells,ndust)
     !ADD ADDITIONAL ATTRIBUTES HERE
   contains
     !>methods
     procedure::init=>init
     procedure::initDensityProfile=>init_density_profile
     procedure::solveChemistry=>solve_chemistry
     procedure::getDensity=>get_density
     procedure::getRadius=>get_radius
     !chemistry
     procedure::initChemistry=>init_chemistry
     !pseudo-hydro
     procedure::evolveHydro=>evolve_hydro
     !radiation
     procedure::setTeffStar=>set_Teff_star
     procedure::setRadiationSource=>set_radiation_source
     procedure::setRadiationMetric=>set_radiation_metric
     procedure::fluxBB=>fluxBB
     procedure::evolveRadiation=>evolve_radiation
     !IO
     procedure::dumpDensityProfile=>dump_density_profile
     procedure::loadParametersFromFile=>load_parameters_from_file
     procedure::dumpRadiationFlux=>dump_radiation_flux
     !ADD ADDITIONAL METHODS HERE
  end type proto

  !class interface
  interface proto
     module procedure new_proto
  end interface proto

contains

  !********************
  !class constructor
  function new_proto()
    implicit none
    type(proto)::new_proto

    !initialize class attributes
    new_proto%density(:) = 0d0 !g/cm3
    new_proto%energy(:) = 0d0 !erg
    new_proto%radius(:) = 0d0 !cm
    new_proto%x(:,:) = 0d0 !#
    new_proto%Jflux(:,:) = 0d0 !eV/cm2
    new_proto%opacity(:,:) = 1d-1 !#
    new_proto%xdust(:,:) = 0d0 !1/cm3
    new_proto%Tdust(:,:) = 0d0 !K
    new_proto%adiabaticIndex(:) = 5d0/3d0
    new_proto%mu(:) = 1.2d0

  end function new_proto

  !include external module for the exercise
  include "proto_mods.f90"

  !*******************
  subroutine load_parameters_from_file(this,fileName)
    implicit none
    class(proto),intent(inout)::this
    character(len=*),intent(in)::fileName
    character(len=30)::fmt
    real*8::accretionRate,starMass,radiusMin,radiusMax
    real*8::starRadius,energyMin,energyMax,dust2gas,gasEnergy
    integer::ios
    namelist /data/ accretionRate,starMass,radiusMin,radiusMax, &
         starRadius,energyMin,energyMax,dust2gas,gasEnergy

    !open namelist file
    open(33,file=fileName,iostat=ios)
    if(ios/=0) then
       print *,"ERROR: can't read file ",trim(fileName)
       stop
    end if
    read(33,data)
    if(ios/=0) then
       print *,"ERROR: problem reading ",trim(fileName)
       stop
    end if
    close(33)

    print *,"************"
    print *,"reading from ",trim(fileName)

    !define accretion rate
    this%accretionRate = accretionRate*msun2g/yr2s
    !mass of the star
    this%starMass = starMass*msun2g
    !simulation box limits
    this%radiusMin = radiusMin*au2cm
    this%radiusMax = radiusMax*au2cm
    !radius of the star
    this%starRadius = starRadius*rsun2cm
    !radiation binning limits
    this%energyMin = energyMin !eV
    this%energyMax = energyMax !eV
    !dust to gas ratio
    this%dust2gas = dust2gas
    !energy per mass (erg/g)
    this%energy(:) = gasEnergy

    fmt = "(a40,99E17.8)"

    print fmt,"Accretion rate (g/s)",this%accretionRate
    print fmt,"Star mass (g)",this%starMass
    print fmt,"Simulation box (cm)",this%radiusMin,this%radiusMax
    print fmt,"Star radius (cm)",this%starRadius
    print fmt,"Radiation energy limits (eV)",this%energyMin,this%energyMax
    print fmt,"Energy per mass (all cells, erg/g)",this%energy(1)
    print fmt,"Dust to gas mass ratio",this%dust2gas
    print *,"************"

  end subroutine load_parameters_from_file

  !*******************
  !wrap all the init subroutine
  subroutine init(this,fileName)
    implicit none
    class(proto),intent(inout)::this
    character(len=*),intent(in)::fileName

    !load parameters from file (namelist)
    call this%loadParametersFromFile(fileName)
    !init density profile
    call this%initDensityProfile()
    !set radiation energy bin
    call this%setRadiationMetric()
    !set effective temperature of the star
    call this%setTeffStar()

  end subroutine init

  !********************
  !initialize density profile and radius in the range
  ! rmin, rmax, accretionRate, mass of the star.
  ! density profile exponent is optional.
  ! all units are cgs
  subroutine init_density_profile(this)
    implicit none
    class(proto),intent(inout)::this
    real*8::rminLog,rmaxLog,r,rexp,mdot,mstar,dl,rr
    integer::i

    rexp = -1.5
    mdot = this%accretionRate
    mstar = this%starMass

    !compute log of radius limits
    rminLog = log10(this%radiusMin)
    rmaxLog = log10(this%radiusMax)

    !loop on cells to compute radius and density
    ! density follows equation (10.34) of Stahler+Palla (2005)
    do i=1,ncells
       !compute radius
       this%radius(i) = 1d1**((i-1)*(rmaxLog-rminLog) &
            / (ncells-1) + rminLog)
       !compute density
       this%density(i) = mdot/4d0/pi/sqrt(2d0*gravity*mstar) &
            * this%radius(i)**rexp
    end do

    !loop on cells to compute grid spacing
    do i=1,ncells-1
       dl = log10(this%radius(i))+log10(this%radius(i+1))
       rr = 1d1**(dl/2d0)
       this%dr(i) = 2d0*(rr-this%radius(i))
    end do
    this%dr(ncells) = 2d0*(this%radius(i)-rr)

    print *,"density profile set!"

  end subroutine init_density_profile

  !********************
  !set species mass fractions of cell with index icell
  subroutine set_chemistry(this,x,icell)
    implicit none
    class(proto),intent(inout)::this
    real*8,intent(in)::x(nchemistry)
    integer,intent(in)::icell

    this%x(icell,:) = x(:)

  end subroutine set_chemistry

  !********************
  !get species mass fractions of the cell with index icell
  function get_chemistry(this,icell)
    implicit none
    class(proto),intent(inout)::this
    integer,intent(in)::icell
    real*8::get_chemistry(nchemistry)

    get_chemistry(:) = this%x(icell,:)

  end function get_chemistry

  !********************
  !get density profile (g/cm3) of all cells
  function get_density(this)
    implicit none
    class(proto),intent(in)::this
    real*8::get_density(ncells)

    get_density(:) = this%density(:)

  end function get_density

  !********************
  !get absolute position (cm) of all cells from
  ! the central source
  function get_radius(this)
    implicit none
    class(proto),intent(in)::this
    real*8::get_radius(ncells)

    get_radius(:) = this%radius(:)

  end function get_radius

  !************************
  !set effective temperature of a protostar assuming
  ! accretion mass mdot, size mstar, and radius rstar.
  ! cgs units.
  subroutine set_Teff_star(this)
    implicit none
    class(proto),intent(inout)::this
    real*8::mdot,mstar,rstar

    mdot = this%accretionRate
    mstar = this%starMass
    rstar = this%starRadius

    !equation (11.8) from Stahler+Palla (2005)
    this%TeffStar = 7.3d3*(mdot*g2msun/s2yr/1d-5)**.25 &
         * (mstar*g2msun/1d0) &
         * (rstar*cm2rsun/5d0)**(-.75)

    print *,"calculated Teff star (K)", this%TeffStar

  end subroutine set_Teff_star

  !***********************
  !define bin spacing from emin to emax, in eV.
  ! spacing is logarithmic
  subroutine set_radiation_metric(this)
    implicit none
    class(proto),intent(inout)::this
    real*8::logEmin,logEmax
    integer::i

    logEmin = log10(this%energyMin)
    logEmax = log10(this%energyMax)

    !loop on energy bins
    do i=1,nenergy
       !bin left right values
       this%energyLeft(i) = 1d1**((i-1)*(logEmax-logEmin)/nenergy+logEmin)
       this%energyRight(i) = 1d1**(i*(logEmax-logEmin)/nenergy+logEmin)
       !bin size
       this%energyDelta(i) = this%energyRight(i)-this%energyLeft(i)
       !bin mid point (averaged)
       this%energyMid(i) = (this%energyRight(i)+this%energyLeft(i))*0.5d0
    end do

    print *,"radiation metric set!"

  end subroutine set_radiation_metric

  !************************
  !define a radiation source from a shell of radius
  ! sourceRadius, with an embedded star of radius starRadius.
  ! this assumes first cell as radiation source emitter.
  ! effective temperature is defined by function setTeffStar.
  ! units are cgs
  subroutine set_radiation_source(this)
    implicit none
    class(proto),intent(inout)::this
    real*8::sourceRadius,starRadius
    real*8::eL,eR,fL,fR
    integer::i

    sourceRadius = this%radiusMin
    starRadius = this%starRadius

    !loop on energy bins
    do i=1,nenergy
       eL = this%energyLeft(i)
       eR = this%energyRight(i)
       fL = fluxBB(this,eL,this%TeffStar)
       fR = fluxBB(this,eR,this%TeffStar)
       this%Jflux(1,i) = pi*(fL+fR)/2d0*starRadius**2/sourceRadius**2
    end do

  end subroutine set_radiation_source

  !*************************
  !propagate radiation cell by cell and for each energy bin
  ! using opacity and geometrical attenuation
  subroutine evolve_radiation(this)
    implicit none
    class(proto),intent(inout)::this
    integer::i,icell
    real*8::tau(nenergy)

    !loop on energy bins
    do i=1,nenergy
       !loop on cells
       do icell=2,ncells
          this%Jflux(icell,i) = this%Jflux(icell-1,i) &
               * exp(-this%opacity(icell,i)) &
               * this%radius(icell-1)**2 / this%radius(icell)**2
       end do
    end do

  end subroutine evolve_radiation

  !*************************
  !spectral radiance (eV/cm2/sr) of a black-body
  ! with temperature Tbb, at a given energy (eV)
  function fluxBB(this,energy,Tbb)
    implicit none
    class(proto),intent(in)::this
    real*8::fluxBB,xexp
    real*8,intent(in)::energy,Tbb

    !exponent
    xexp = energy/kboltzmann_ev/Tbb

    !default value
    fluxBB = 0d0

    !limit exp overflow
    if(xexp<3d2.and.energy>1d-10) then
       fluxBB = 2d0*energy**3 / &
            hplanck_eV**2 / &
            clight**2 &
            / (exp(xexp)-1d0)
    end if

  end function fluxBB

  ! ****************
  ! add PdV heating
  subroutine evolve_hydro(this, dt)
    use proto_commons
    implicit none
    class(proto),intent(inout)::this
    real*8,intent(in)::dt
    real*8::heat, tff
    integer::i

    ! loop on cells to compute PdV heating
    do i=1,ncells
       ! free-fall time
       tff = sqrt(3d0 * pi / 32d0 / gravity / this%density(i))
       ! compressional heating
       heat = (this%adiabaticIndex(i) - 1d0) * this%energy(i) * dt / tff
       ! add energy density from heating
       this%energy(i) = this%energy(i) + heat
    end do

  end subroutine evolve_hydro

end module proto_class
